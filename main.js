import {
  favoriteClick,
  addToFavorite,
  removeFavorite,
  removeUploaded,
  displayUploaded,
  debounce,
  uploadGifOnClick,
} from './src/event/events.js';

import {
  fetchGifDetails,
  fetchCategorySearch,
  fetchSearch,
  fetchUrlTrending,
  fetchTrendingInfinityScroll,
  fetchSearchInfinityScroll,
} from './src/request/request.js';

let checkState = 'search';
document.addEventListener('DOMContentLoaded', function () {
  /** when the first page is open to load all trending */
  fetchCategorySearch('laughing');
  /**
   * @listens scroll
   * @fires fetchUrlTrendingOffSet()
   * @info endless scrolling
   */
  window.addEventListener(
    'scroll',
    debounce(() => {
      if (
        window.scrollY + window.innerHeight >=
        document.documentElement.scrollHeight
      ) {
        if (checkState === 'home' || checkState === 'trending') {
          fetchTrendingInfinityScroll();
        }
        if (checkState === 'search' || checkState === 'category') {
          fetchSearchInfinityScroll();
        }
      }
    }, 500)
  );

  /**
   * @listens click
   * @fires
   * @event e
   */
  document.addEventListener('click', (e) => {
    /**
     * @event fetchUrlTrending()
     * @fires fetchUrlTrending()
     * Trending event
     */
    if (e.target.id === 'trending' || e.target.id === 'image-trending') {
      checkState = 'trending';
      fetchUrlTrending();
    }

    /**
     * @event fetchUrlTrending()
     * @fires fetchUrlTrending()
     * return to the main page 'trending'
     */
    if (e.target.id === 'change') {
      checkState = 'change';
      fetchUrlTrending();
    }

    /**
     * @event uploadGifOnClick()
     * @fires uploadGifOnClick()
     * Upload event
     */
    if (e.target.id === 'upload' || e.target.id === 'image-upload') {
      uploadGifOnClick();
    }

    /**
     * @event displayUploaded()
     * @fires displayUploaded()
     * Display Uploaded event
     */
    if (
      e.target.id === 'display-uploads' ||
      e.target.id === 'image-display-uploaded'
    ) {
      checkState = 'uploaded';
      displayUploaded();
    }

    /**
     * @event removeUploaded()
     * @fires removeUploaded()
     * Remove from uploaded event
     */
    if (e.target.id === 'remove-uploaded') {
      removeUploaded();
    }

    /**
     * @event favoriteClick()
     * @fires favoriteClick()
     * Open favorite gif's
     * Favorite event
     */
    if (e.target.id === 'favorite' || e.target.id === 'image-favorite') {
      checkState = 'favorite';
      favoriteClick();
    }

    /**
     * @event addToFavorite(e)
     * @fires addToFavorite(e)
     *  @param {object} e - allowing to manipulate dynamically
     */
    if (e.target.id === 'like') {
      addToFavorite(e);
    }

    /**
     * @event removeFavorite(e)
     * @fires removeFavorite(e)
     * @param {object} e - allowing to manipulate dynamically
     */
    if (e.target.id === 'unlike') {
      removeFavorite(e);
    }

    /**
     * @event search()
     * @fires search()
     */
    if (
      (e.target.id === 'button-search' ||
        e.target.id === 'image-button-search') &&
      document.querySelector('#search').value !== ''
    ) {
      checkState = 'search';
      fetchSearch();
    }

    /**
     * @event displayGifDetails(e.target.id)
     * @fires displayGifDetails
     * @param {string} e.target.id
     *
     * @event fetchCategorySearch(e.target.getAttribute('data-categories'))
     * @fires fetchCategorySearch
     * @param {string}
     */
    if (e.target.tagName === 'IMG') fetchGifDetails(e.target.id);

    if (e.target.classList.contains('category-click')) {
      checkState = 'category';
      e.preventDefault();
      fetchCategorySearch(e.target.getAttribute('data-categories'));
    }
  });
});
