import { q } from '../../utils/utils.js';
import { emptyPageUploaded } from '../../utils/visualization.js';
import {
  fetchUploaded,
  fetchFavorite,
  fetchRandomGif,
  uploadGif,
} from '../request/request.js';

/**
 * @info remove favorite gif
 * @param {Object} e - object event, allow to access the correct element
 */
export const removeFavorite = (e) => {
  const id = e.path[0].nextElementSibling.id;
  localStorage.setItem('favorite', getTheIdForRemove(id));
  const data = JSON.parse(localStorage.getItem('favorite'));

  if (data === null) return fetchRandomGif();
  else {
    const element = document.getElementById(e.path[1].id);
    return element.remove();
  }
};

/**
 * @info manipulate localStorage to be on track
 * @param {string} id - id of the gif as a string
 * @returns {arrStringy} string
 */
export const getTheIdForRemove = (id) => {
  const arrayFav = JSON.parse(localStorage.getItem('favorite'));
  const removedFav = arrayFav.filter((data) => data !== id);
  if (removedFav.length === 0) {
    return null;
  } else {
    const arrStringy = JSON.stringify(removedFav);
    return arrStringy;
  }
};

/**
 * @info on favorite click
 * @returns {random gif or all favorite gif's}
 */
export const favoriteClick = () => {
  const data = JSON.parse(localStorage.getItem('favorite'));
  if (data === null) {
    return fetchRandomGif();
  } else {
    return fetchFavorite();
  }
};

/**
 * @info add gif to favorite section
 * @param {Object} e - object event, allow to access the correct element
 */
export const addToFavorite = (e) => {
  const id = e.path[0].nextElementSibling.id;
  localStorage.setItem('favorite', getTheIdForAddToFavorite(id));
};

/**
 * @info manipulate localStorage to be on track
 * @param {string} id - string to manipulate localStorage
 * @returns {arrStringy} string
 */
export const getTheIdForAddToFavorite = (id) => {
  let arrayFav = JSON.parse(localStorage.getItem('favorite'));
  if (arrayFav === null) {
    arrayFav = [id];
  } else if (!arrayFav.includes(id)) {
    arrayFav.push(id);
  } else {
    // alert('Already added to Favorite :)');
  }

  const arrStringy = JSON.stringify(arrayFav);
  return arrStringy;
};

/**
 * @info display uploaded section
 * @returns emptyPage or all uploaded gif's
 */
export const displayUploaded = () => {
  const data = JSON.parse(localStorage.getItem('gifs'));
  if (data === null || data.length === 0) {
    return emptyPageUploaded();
  } else {
    return fetchUploaded();
  }
};

/**
 * @info remove uploaded gif
 * @returns if storage is empty will return emptyPage
 */
export const removeUploaded = () => {
  const getClass = q('.container-img');
  const element = getClass.getAttribute('id');
  const uploaded = JSON.parse(localStorage.getItem('gifs'));

  const removeFromStorage = uploaded.filter((id) => id !== element);
  localStorage.setItem('gifs', JSON.stringify(removeFromStorage));
  getClass.remove();

  if (removeFromStorage.length === 0) {
    return emptyPageUploaded();
  }
};

/**
 * @info add uploaded gif to localStorage
 * @param {string} uploadedGif - id of the uploaded gif
 */
export const addUploadedGifToLocalStorage = (uploadedGif) => {
  let parsedStorage = JSON.parse(localStorage.getItem('gifs'));
  if (parsedStorage === null) {
    parsedStorage = [uploadedGif];
  } else {
    parsedStorage.push(uploadedGif);
  }
  localStorage.setItem('gifs', JSON.stringify(parsedStorage));
};

/**
 * @info you can upload your own gif
 */
export const uploadGifOnClick = () => {
  const input = q('#fileInput');
  input.click();
  input.onchange = (e) => {
    let file = e.target.files[0];
    uploadGif(file);
  };
};

/**
 * @info prevent geting more then 1 fetch in a 0.5 sec
 * @param {function} fn
 * @param {number} delay
 * @returns
 */
export const debounce = (fn, delay) => {
  let timeout; // keep in a closure
  return function () {
    // the closure func
    if (timeout) clearTimeout(timeout);
    timeout = setTimeout(() => {
      fn();
    }, delay);
  };
};
