import {
  apiForCategories,
  apiTrending,
  apiTrendingForScroll,
  displayApi,
  randomApi,
  searchApi,
  uploadApi,
} from '../../constants/constants.js';
import {
  displayFavorite,
  displayUploaded,
  showViewRandomLaughy,
  visualizeGiphys,
  visualizeGiphysForInfinityScroll,
} from '../../utils/visualization.js';
import { apiDetails } from '../../constants/constants.js';
import { q } from '../../utils/utils.js';
import { displayGiphy } from '../../utils/visualization.js';
import { addUploadedGifToLocalStorage } from '../event/events.js';

let inputEndless = '';

export const fetchCategorySearch = async (id) => {
  inputEndless = id;
  const url = apiForCategories + id;
  const response = await fetch(url);
  const content = await response.json();
  visualizeGiphys(content.data);
};

/**
 * @async
 * @param {string} imgId  - id as a string
 */
export const fetchGifDetails = async (imgId) => {
  const api = apiDetails(imgId);
  const response = await fetch(api);
  const content = await response.json();
  'abfnfmgfjgk'.substring(0, 9);
  q('.pictures').innerHTML = displayGiphy(content.data);
};

/**
 * @async
 */
export const fetchUploaded = async () => {
  let uploaded = JSON.parse([localStorage.getItem('gifs')]).join(',');
  let api = displayApi + uploaded;
  const response = await fetch(api);
  const content = await response.json();

  displayUploaded(content.data);
};

/**
 * @async
 */
export const fetchFavorite = async () => {
  let favorite = JSON.parse([localStorage.getItem('favorite')]).join(',');
  let api = displayApi + favorite;
  const response = await fetch(api);
  const content = await response.json();

  displayFavorite(content.data);
};

/**
 * @async
 */
export const fetchRandomGif = async () => {
  const response = await fetch(randomApi);
  const content = await response.json();
  showViewRandomLaughy(content);
};

/**
 * @async
 */
export const fetchSearch = async () => {
  const input = document.querySelector('#search');
  inputEndless = input.value;
  const api = searchApi(input.value, 0);
  const response = await fetch(api);
  const content = await response.json();

  if (content.data.length === 0) {
    alert('there is no such user or gifs');
    input.value = '';
  } else {
    input.value = '';
    visualizeGiphys(content.data);
  }
};

/*  */
let counterSearch = 0;
export const fetchSearchInfinityScroll = async () => {
  counterSearch += 25;
  const api = searchApi(inputEndless, counterSearch);
  const response = await fetch(api);
  const content = await response.json();
  visualizeGiphysForInfinityScroll(content.data);
};

/**
 * @async
 */
export const fetchUrlTrending = async () => {
  const response = await fetch(apiTrending);
  const content = await response.json();
  visualizeGiphys(content.data);
};

let counterTrending = 0;
export const fetchTrendingInfinityScroll = async () => {
  counterTrending += 25;
  const response = await fetch(apiTrendingForScroll(counterTrending));
  const content = await response.json();
  visualizeGiphysForInfinityScroll(content.data);
};

/**
 * @async
 * @param {gif} file - only gif file will be uploaded
 */
export const uploadGif = async (file) => {
  const formData = new FormData();
  formData.append('file', file);
  if (formData.get('file').type !== 'image/gif')
    return alert('please select a gif');
  const url = uploadApi;
  const response = await fetch(url, { method: 'POST', body: formData });
  const uploadedGif = (await response.json()).data.id;
  addUploadedGifToLocalStorage(uploadedGif);
  return alert('upload was sucsessfull');
};

/**
 * @async
 */
