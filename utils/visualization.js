import { q } from './utils.js';


/**
 * @info for search and trending 
 * @param {object} data as a object
 * @returns html template
 */
export const createGiphy = (data) => {
  return ` 
    <div class="container-img" id="img">
        <button class="like-button" id="like" title="Add to favorite">
          &#x2665
        </button>
        <img class="gify" src="${data.images.fixed_height_downsampled.url}" alt="${data.title}" id="${data.id}">
    </div> 
        `;
};

/**
 * @info display gify details
 * @param {Object} data  as a object
 * @returns html template
 */
const toGifDetailed = (data) => {
  const date = data.trending_datetime;
  return `
  <div id= 'infoGif'>
  <a href="${data.url}" target="_blank">link to origin</a> 
    <p>Title: ${data.title}</p>
    <p> Date: ${date.substring(0, 10)}</p>
    <p> Time: ${date.substring(11)}</p>
    <p>Type: ${data.type}</p>
    <p>Username: ${data.username}</p>
  </div>
`;
};


/**
 * @info display gify details
 * @param {Object} data={}
 * @returns html template
 */
export const displayGiphy = (data) => {
  const view = `
    <div>
      <h3>${data.title}</h3>
      <div class="gifs-section">
        <img src="${data.images.fixed_height_downsampled.url}" alt="${
    data.title
  }" id="${data.id}" class='gif'>
        ${toGifDetailed(data)}
        </div>
      </div>
    </div>`;

  return `<div id = 'allInfo'>${view}</div>`;
};

/**
 * @info for search i trending 
 * @param {Array} pictureArray=[] 
 * @returns create html gif for every element in the array
 */
export const visualizeGiphys = (pictureArray) => {
  const holder = q('.pictures');
  const createdHtml = pictureArray.reduce(
    (images, data) => (images += createGiphy(data)),
    ''
  );
  holder.innerHTML = createdHtml;
};

/**
 * @info for infinity scroll
 * @param {Array} pictureArray=[]
 * @returns created gif for every element in the array
 */
export const visualizeGiphysForInfinityScroll = (pictureArray) => {
  const holder = q('.pictures');
  pictureArray.forEach((el) => (holder.innerHTML += createGiphy(el)));
};

/**
 * @info create upload gif
 * @param {Object} data as a object
 * @param {String} id as a string
 * @returns html template 
 */
export const createUploadGif = (data, id) => {
  return `
  <div class="container-img" id="${id}">
      <button class="like-button" id="remove-uploaded">&times;</button>
      <img src="${data.images.fixed_height_downsampled.url}" id="${id}">
  </div>
      `;
};

/**
 * @info display all uploaded
 * @param {Array} pictureArray=[]
 */
export const displayUploaded = (pictureArray) => {
  const holder = q('.pictures');
  const createdHtml = pictureArray.reduce((images, data) => {
    images += createUploadGif(data, data.id);
    return images;
  }, '');
  holder.innerHTML = createdHtml;
};

/**
 * @info if empty create NO items found
 * @returns html template for No uploaded items
 */
export const emptyPageUploaded = () => {
  const holder = q('.pictures');

  return (holder.innerHTML = `
    <div class="container-img">
      <p class="uploaded" id="remove">No uploaded items</p>
    </div>
      `);
};

/**
 * @info create gifs with (x) in favorite
 * @param {Object} data as a object
 * @param {String} id as a string
 * @returns html template 
 */
const createLikedGif = (data, id) => {
  return `
    <div class="container-img" id="${id}">
        <button class="like-button" id="unlike">&times;</button>
        <img src="${data.images.fixed_height_downsampled.url}" id="${id}">
    </div>
        `;
};


/**
 * @info display all favorite 
 * @param {Array} pictureArray=[]
 */
export const displayFavorite = (pictureArray) => {
  const holder = q('.pictures');
  const createdHtml = pictureArray.reduce((images, data) => {
    images += createLikedGif(data, data.id);
    return images;
  }, '');
  holder.innerHTML = createdHtml;
};

/**
 * @info create random gif html template
 * @param {Object} data 
 * @returns html template for Random gif
 */
const createRandomLaughy = (data) => {
  return `
  <div id="favorite-gif">
      <p id="favoriteP"> You don't have any favorite laughy, Get your random one</p><br>
      <img src="${data.images.fixed_height_downsampled.url}" alt="${data.name}">
  </div>`;
};
/**
 * @info random gif
 * @param {Object} content 
 */
export const showViewRandomLaughy = (content) => {
  const buttonF = q('.pictures');
  const gif = createRandomLaughy(content.data);
  buttonF.innerHTML = gif;
};
