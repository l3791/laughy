/**
 * @param {string} selector  
 * @returns {document.querySelector(selector)}
 */
export const q = (selector) => document.querySelector(selector);

/**
 * @param {string} selector  
 * @returns {Array} array
 */
export const qs = (selector) => Array.from(document.querySelectorAll(selector));
