export const apiTrendingForScroll = (offset) =>
  `https://api.giphy.com/v1/gifs/trending?&api_key=zqhyY5IcW4btgHm7cIg9Efi6hWnSZWky&limit=25&offset=${offset}`;

export const apiForCategories = `https://api.giphy.com/v1/gifs/search?&api_key=zqhyY5IcW4btgHm7cIg9Efi6hWnSZWky&q=`;

export const apiDetails = (id) =>
  `https://api.giphy.com/v1/gifs/${id}?api_key=zqhyY5IcW4btgHm7cIg9Efi6hWnSZWky`;

export const displayApi =
  'https://api.giphy.com/v1/gifs?&api_key=zqhyY5IcW4btgHm7cIg9Efi6hWnSZWky&ids=';

export const randomApi =
  'https://api.giphy.com/v1/gifs/random?&api_key=zqhyY5IcW4btgHm7cIg9Efi6hWnSZWky';

export const searchApi = (input, offset) =>
  `https://api.giphy.com/v1/gifs/search?&api_key=zqhyY5IcW4btgHm7cIg9Efi6hWnSZWky&q=${input}&limit=25&offset=${offset}`;

export const apiTrending =
  'https://api.giphy.com/v1/gifs/trending?&api_key=zqhyY5IcW4btgHm7cIg9Efi6hWnSZWky&limit=25';

export const uploadApi =
  'https://upload.giphy.com/v1/gifs?api_key=zqhyY5IcW4btgHm7cIg9Efi6hWnSZWky';
