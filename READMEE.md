# Laughy

## Description

Contains an input field (at the top of the web page) in which you can search by full or partial title or username. After pressing the magnifying glass button, it displays all the gifs filtered by the specified criteria.

If the user's requested gifs are not found, a notification will be displayed (a small window at the top of the web page in which a message will be displayed)

On each button, if hovered, a text will be pop-up and will tell the user what the particular button is used for.

Information for each gif can be displayed after clicking on it.
To return to the original page it is necessary to click somewhere outside the additional information.

We can go back to the previous page we were on, not the home page.

<br>

## Support

<br>

**Trending** -- will show current trending gif's.
<img src ='./images/A.PNG' >
 
 <br>

**Favorite** -- gives you opportunity to keep your favorite gif's in separate bucked, and delete them if you don't want to be your favorite any more.
<img src ='./images/B.PNG' >

<br>

**Gif details** -- display the gif detail, as title, Date of upload, time, type and username who uploaded. Also provided link to the origin of the gif.
<img src ='./images/gif-details.png' >
 
 <br>

**Category** -- You have few categories to use as a short way into search.
<img src ='./images/categories.png' >

<br>

**Upload** -- Possibility to upload your own gif, and display them in **Display Uploads** section. Also you can remove uploaded gif from there, when you click X button.
<img src ='./images/upload.png' >

<br>

## Contributing

<b>constants</b> - common holds resources used by other files, such resources are the constants in constants.js.

<b>css</b> - files style.css and style1.css  contains the CSS files included in the app

<b>img</b> - contains the images for styling the page

<b>src</b> - inside you can find folders event (event.js), request (request.js)
  - <b>event /event.js</b> - holds the core app logic. Event listeners in main.js use directly functions exposed in the events folder files.
  - <b>request/ request.js</b> - provid access to the public API of the data layer.

<b>utils</b> - contains several files (utils.js, visualization.js) whose functions are reused.

- <b>utils.js</b> has some helpers methods including aliasing for document.querySelector and document.querySelectorAll. 
  
- <b>visualization.js</b> -  has 12 methods for visualization.
<b>
	- createGiphy() 

	- toGifDetailed()

	- displayGiphy()

	- visualizeGiphys()

	- visualizeGiphysForInfinityScroll()

	- createUploadGif()

	- displayUploaded()

	- emptyPageUploaded()

	- createLikedGif()

	- displayFavorite()

	- createRandomLaughy()

	- showViewRandomLaughy()
</b>

<b>index.html</b> - is our initial html template, that we add all other functional visualization dynamically.

<b>main.js</b> - handle and trigger our events,  depends from the user interaction